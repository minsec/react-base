import React from 'react'

const Public = () => <h3>Public</h3>
const Protected = () => <h3>Protected</h3>

export { Public, Protected }