import React, { Component } from "react"
import { Redirect } from "react-router-dom";

class Login extends Component {
    state = {
        redirectToReferer: false
    }

    login = () => {
        fakeAuth.authenticate(() => {
            this.setState({ redirectToReferer: true })
        })
    }

    render() {
        const { from } = this.props.location.state || { from: { pathname: '/' } }
        const { redirectToReferer} = this.state

        if (redirectToReferer) {
            return (
                <Redirect to={ from } />
            )
        }
        return (
            <div>
                <p>You must log in to view the page at { from.pathname }</p>
                <button onClick={this.login}>Log in</button>
            </div>
        );
    }
}

export default Login;